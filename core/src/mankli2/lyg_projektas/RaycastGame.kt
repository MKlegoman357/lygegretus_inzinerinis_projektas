package mankli2.lyg_projektas

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.graphics.glutils.FrameBuffer
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Polygon
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer
import com.badlogic.gdx.physics.box2d.Fixture
import com.badlogic.gdx.physics.box2d.World
import ktx.app.KtxApplicationAdapter
import ktx.app.clearScreen
import ktx.box2d.body
import ktx.box2d.createWorld
import ktx.box2d.rayCast
import ktx.graphics.color
import ktx.graphics.use

data class RayCastHit(
		val fixture: Fixture,
		val point: Vector2,
		val normal: Vector2,
		val fraction: Float,

		val start: Vector2,
		val end: Vector2
)

data class Box(
		val width: Float,
		val height: Float,

		val x: Float = 0f,
		val y: Float = 0f,

		val rotation: Float = 0f
) {

	val polygon by lazy {
		Polygon(floatArrayOf(
				0f, 0f,
				width, 0f,
				width, height,
				0f, height
		)).apply {
			setOrigin(width / 2, height / 2)
			setPosition(x, y)
			rotate(rotation)
		}
	}
}

class RaycastGame : KtxApplicationAdapter {
	private lateinit var spriteBatch: SpriteBatch
	private lateinit var shape: ShapeRenderer
	private lateinit var shapeFrameBuffer: FrameBuffer
	private lateinit var img: Texture

	private lateinit var camera: Camera

	private lateinit var debugRenderer: Box2DDebugRenderer
	private lateinit var world: World

	private var x = 0f
	private var y = 0f
	private var speed = 100f

	private val deltaTime get() = Gdx.graphics.deltaTime

	private val boxes = listOf(
			Box(100f, 100f, 0f, 0f, -45f),
			Box(200f, 100f, 300f, 300f, -45f)
	)

	override fun create() {
		spriteBatch = SpriteBatch()
		shape = ShapeRenderer()
		shapeFrameBuffer = FrameBuffer(Pixmap.Format.RGBA8888, Gdx.graphics.width, Gdx.graphics.height, false)
		img = Texture("badlogic.jpg")

		camera = OrthographicCamera(Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())
		camera.position.set(Gdx.graphics.width / 2f, Gdx.graphics.height / 2f, 0f)
		camera.update()

		debugRenderer = Box2DDebugRenderer()
		world = createWorld()

		for (box in boxes) {
			world.body {
				box(box.width, box.height, Vector2(box.x, box.y), box.rotation * MathUtils.degreesToRadians)
			}
		}
	}

	override fun render() {
		input()

		clearScreen(.5f, .5f, 1.0f)

		shapeFrameBuffer.use {
			shape.use(ShapeRenderer.ShapeType.Filled) {
				shape.color = color(1f, 0.5f, 0.5f)

				boxes.forEach {
					shape.identity()
					shape.rect(
							it.x - it.width / 2, it.y - it.height / 2,
							it.width / 2, it.height / 2,
							it.width, it.height,
							1f, 1f,
							it.rotation
					)
				}
			}
		}

		spriteBatch.use {
			spriteBatch.draw(TextureRegion(shapeFrameBuffer.colorBufferTexture).apply {
				flip(false, true)
			}, 0f, 0f)
		}

		debugRenderer.render(world, camera.combined)

		rayCast()?.let { hit ->
			shape.use(ShapeRenderer.ShapeType.Filled) {
				shape.color = color(1f, 0.9f, 0.9f)
				shape.rectLine(hit.start, hit.point, 5f)
			}
		}
	}

	private fun rayCast(): RayCastHit? {
		val start = Vector2(camera.viewportWidth / 2, camera.viewportHeight / 2)
		val end = Vector2(camera.viewportWidth, camera.viewportHeight)

		var hit: RayCastHit? = null

		world.rayCast(start, end) { fixture: Fixture, point: Vector2, normal: Vector2, fraction: Float ->
			if (hit == null || fraction < hit!!.fraction)
				hit = RayCastHit(fixture, point, normal, fraction, start, end)
			fraction
		}

		return hit
	}

	private fun input() {
		if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) x -= speed * deltaTime
		if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) x += speed * deltaTime
		if (Gdx.input.isKeyPressed(Input.Keys.UP)) y += speed * deltaTime
		if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) y -= speed * deltaTime
	}

	override fun dispose() {
		spriteBatch.dispose()
		img.dispose()
		shape.dispose()
		shapeFrameBuffer.dispose()
		debugRenderer.dispose()
	}
}