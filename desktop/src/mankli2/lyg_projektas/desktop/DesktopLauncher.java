package mankli2.lyg_projektas.desktop;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import mankli2.lyg_projektas.RaycastGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();

		config.setTitle("Raycast game");
		config.setWindowedMode(480, 320);

		new Lwjgl3Application(new RaycastGame(), config);
	}
}
